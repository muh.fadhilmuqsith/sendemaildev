using Novu.Models;
using Novu;
using Novu.DTO.Events;
using System.Net.Mail;
using System.Text.Json;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

var summaries = new[]
{
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

app.MapGet("/weatherforecast", () =>
{
    var forecast = Enumerable.Range(1, 5).Select(index =>
        new WeatherForecast
        (
            DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            Random.Shared.Next(-20, 55),
            summaries[Random.Shared.Next(summaries.Length)]
        ))
        .ToArray();
    return forecast;
})
.WithName("GetWeatherForecast")
.WithOpenApi();


app.MapPost("/sendEmail", async (HttpContext context) =>
{
    if (context.Request.HasFormContentType)
    {
        var form = await context.Request.ReadFormAsync();

        // Access the file from the form collection
        var files = form.Files["file"];
        var base64code = "";
        using (MemoryStream memoryStream = new MemoryStream())
        {
            files.CopyTo(memoryStream);
            byte[] fileBytes = memoryStream.ToArray();

            // Convert the byte array to Base64 string
            string base64String = Convert.ToBase64String(fileBytes);

            base64code = base64String;
        }



        var novuConfiguration = new NovuClientConfiguration
        {
            // Defaults to https://api.novu.co/v1
            Url = "https://api.novu.co/v1",
            ApiKey = "f6fd3c2f7fed1f7047ef39f8eff4cd12",
        };

        var novu = new NovuClient(novuConfiguration);

        var listAttachment = new List<PayloadEmail>();
        var opt = new JsonSerializerOptions() { WriteIndented = true };
       
        listAttachment.Add(new PayloadEmail()
        {
            file = base64code,
            name = files.FileName,
            mime = files.ContentType

        });

        var payloadData = new
        {
            name = "TESTTING",
            attachments = listAttachment

        };

        var payload = new EventCreateData()
        {
            EventName = "testing",
            To = { SubscriberId = "1234567", Email = "mfmowman25@gmail.com" },
            Payload = payloadData
        };

        var trigger = await novu.Event.Trigger(payload);

        return Results.Ok(trigger);
    }
    return Results.Ok();
})
.WithName("SendEmail")
.WithOpenApi();



app.Run();
public class PayloadEmail
{
    public string file { get; set; }
    public string mime { get; set; }
    public string name { get; set; }
}
internal record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}
